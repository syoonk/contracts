// SPDX-License-Identifier: MIT

pragma solidity ^0.7.3;

import "@openzeppelin/contracts/token/ERC20/ERC20Capped.sol";

contract Token is ERC20Capped {
  
  uint constant TOTAL_SUPPLY = 10**7 * 10**18;
  
  constructor(address _tokenReceiver)
  ERC20Capped(TOTAL_SUPPLY)
  ERC20("Capped Token Standard", "CTS") {
    _mint(_tokenReceiver, TOTAL_SUPPLY); 
  }
  
}