const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");

const Token = artifacts.require("Token");

const toBN = (_amount) => {
  return web3.utils.toBN(String(_amount));
}

const toWei = (_amount) => {
  return web3.utils.toWei(String(_amount));
}

const bigWei = (_amount) => {
  return toBN(toWei(_amount));
}

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

const TOTAL_SUPPLY = toWei(String(10**7));

contract("CappedToken", accounts => {
  let token;
  
  beforeEach(async () => {
    token = await Token.new(accounts[1]);
  });

  it("should deployed properly", async () => {
    const deployerBalance = await token.balanceOf(accounts[0]);
    const receiverBalance = await token.balanceOf(accounts[1]);

    assert.equal(deployerBalance, 0);    
    assert(receiverBalance.eq(toBN(TOTAL_SUPPLY)));
  });

  it("should NOT mint again", async () => {
    try {
      await token._mint(accounts[0], 1, { from: accounts[0] });
    } catch(err) {
    }
  });

  it("should transfer", async () => {
    const receipt = await token.transfer(accounts[0], 100, { from: accounts[1] });
    const balance0 = await token.balanceOf(accounts[0]);
    const balance1 = await token.balanceOf(accounts[1]);
    
    expectEvent(receipt, "Transfer", { from: accounts[1], to: accounts[0], value: "100" });
    assert.equal(Number(balance0), 100);
    assert(toBN(TOTAL_SUPPLY).sub(toBN("100")).eq(balance1));
  });

  it("should NOT transfer", async () => {
    // invalid address
    await expectRevert(
      token.transfer(ZERO_ADDRESS, 100, { from: accounts[1] }),
      "ERC20: transfer to the zero address"
    );

    // not enough balance
    await expectRevert(
      token.transfer(accounts[0], toBN(TOTAL_SUPPLY).add(toBN("1")), { from: accounts[1] }),
      "ERC20: transfer amount exceeds balance"
    );
    await expectRevert(
      token.transfer(accounts[1], 100, { from: accounts[0] }),
      "ERC20: transfer amount exceeds balance"      
    );
  });

  it("should transferFrom", async () => {
    const receipt_approval = await token.approve(accounts[2], 1000, { from: accounts[1] });
    const allowance_before = await token.allowance(accounts[1], accounts[2]);

    const receipt_transfer = await token.transferFrom(
      accounts[1], 
      accounts[0], 
      1000, 
      { from: accounts[2] }
    );
    const balance0 = await token.balanceOf(accounts[0]);
    const balance1 = await token.balanceOf(accounts[1]);
    const balance2 = await token.balanceOf(accounts[2]);
    const allowance_after = await token.allowance(accounts[1], accounts[2]);

    expectEvent(receipt_approval, "Approval", {owner: accounts[1], spender: accounts[2], value: "1000"});
    expectEvent(receipt_transfer, "Transfer", {from: accounts[1], to: accounts[0], value: "1000"});
    assert.equal(Number(balance0), 1000);
    assert(toBN(TOTAL_SUPPLY).sub(toBN("1000")).eq(balance1));
    assert.equal(balance2, 0);
    assert.equal(Number(allowance_before), 1000);
    assert.equal(Number(allowance_after), 0);
  });

  it("should NOT transferFrom", async () => {
    // not allowed
    await expectRevert(
      token.transferFrom(
        accounts[1], 
        accounts[0], 
        1000, 
        { from: accounts[2] }
      ),
      "ERC20: transfer amount exceeds allowance"
    );

    await token.approve(accounts[2], 1000, { from: accounts[1] });

    // exceeds allowed
    await expectRevert(
      token.transferFrom(
        accounts[1], 
        accounts[0], 
        1001, 
        { from: accounts[2] }
      ),
      "ERC20: transfer amount exceeds allowance"
    );

    await token.transferFrom(
      accounts[1], 
      accounts[0], 
      1000, 
      { from: accounts[2] }
    );

    // allowance depleted
    await expectRevert(
      token.transferFrom(
        accounts[1], 
        accounts[0], 
        1000, 
        { from: accounts[2] }
      ),
      "ERC20: transfer amount exceeds allowance"
    );

    await token.approve(accounts[2], toBN(TOTAL_SUPPLY).add(toBN(TOTAL_SUPPLY)), { from: accounts[1] });

    // exceeds balance
    await expectRevert(
      token.transferFrom(
        accounts[1], 
        accounts[0], 
        toBN(TOTAL_SUPPLY).add(toBN("1")), 
        { from: accounts[2] }
      ),
      "ERC20: transfer amount exceeds balance"
    );

  });
});