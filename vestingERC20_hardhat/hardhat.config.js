require("@nomiclabs/hardhat-ethers");

const url = "https://kovan.infura.io/v3/8a313bcfaadd4f6384598e705151f606";
const prv = "3833e91e92c9d57e6ac4b1f295f9a4d01145d5f29852a1363147b93db2335755";

module.exports = {
  solidity: "0.8.0",
  networks: {
    kovan: {
      url,
      accounts: [prv]
    }
  }
};