const { merge } = require('sol-merger');

// Get the merged code as a string
const mergedCode = merge("./contracts/VestingToken.sol").then(result => console.log(result));
// Print it out or write it to a file etc.
console.log(mergedCode);