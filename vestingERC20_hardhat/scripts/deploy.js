const main = async () => {
  const [deployer] = await ethers.getSigners();

  console.log("Deploying contracts with the contract: ", deployer.address);
  console.log("Account balance: ", (await deployer.getBalance()).toString());

  const Token = await ethers.getContractFactory("TokenBase");
  const token = await Token.deploy("Coinmeca Standard Token", "CMST");

  console.log("Token address: ", token.address);
}

main()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });