pragma solidity ^0.8.0;

import "./TokenBase.sol";

contract VestingToken is TokenBase {

  constructor()
  TokenBase("VestingToken", "VTT") {
    _mint(msg.sender, 10000000 ether);
  }

}