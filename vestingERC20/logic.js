const currentTime = 1616312474; // seconds

function nothing() {
  
}

nothing();

let lockInfo = {};

function lockSet(
  _iterations, 
  _totalAmount, 
  _unitTime,
  _delay
  ) {
  lockInfo = {
    startTime: currentTime + _delay,
    iterations: _iterations,
    totalAmount: _totalAmount,
    unitTime: _unitTime
  };
}

lockSet(10, 10000, 60, 120);

function printCurrentLock(_currentTime) {
  if(lockInfo.startTime > _currentTime) {
    console.log("Remained: ", lockInfo.iterations);
    console.log("Lock Amount: ", lockInfo.totalAmount);
    return ;
  }
  
  const endTime = Number(lockInfo.startTime + (lockInfo.iterations * lockInfo.unitTime));
  const iterRemained = parseInt((endTime - Number(_currentTime)) / lockInfo.unitTime);
  const lockAmount = parseInt(lockInfo.totalAmount * iterRemained / lockInfo.iterations);
  
  console.log("Remained: ", iterRemained);
  console.log("Lock Amount: ", lockAmount);
}