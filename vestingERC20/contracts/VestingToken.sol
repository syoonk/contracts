pragma solidity ^0.7.3;

import "./TokenBase.sol";

contract VestingToken is TokenBase {

  constructor()
  TokenBase("VestingToken", "VTT") {
    _mint(msg.sender, 10000000 ether);
  }

}