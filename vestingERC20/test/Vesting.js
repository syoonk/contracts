const { expectRevert, expectEvent, time } = require("@openzeppelin/test-helpers");

const VestingToken = artifacts.require("VestingToken");

const toBN = (_amount) => {
  return web3.utils.toBN(String(_amount));
}

const toWei = (_amount) => {
  return web3.utils.toWei(String(_amount));
}

const bigWei = (_amount) => {
  return toBN(toWei(_amount));
}

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

const TOTAL_SUPPLY = toWei(String(10**7));

contract("VestingToken", accounts => {
  let token;
  
  beforeEach(async () => {
    token = await VestingToken.new();
  });

  it("should deploy", async () => {
    const ownerBalance = await token.balanceOf(accounts[0]);

    assert(ownerBalance.eq(toBN(TOTAL_SUPPLY)));

    const owner = await token.owner();
    
    assert.equal(owner, accounts[0]);
  });

  it("should lock", async () => {
    const lockAmount = toWei("100");
    await token.setLock(
      accounts[1],
      3600,
      10,
      lockAmount,
      600,
      { from: accounts[0] }
    );

    const lockInfos = await token.getLock(accounts[1]);
    const lockDetails = await token.getLockDetails(accounts[1]);

    console.log("Start time: ", new Date(lockInfos[0].toNumber() * 1000).toLocaleString());
    assert.equal(lockInfos[1].toString(), "10");
    assert(lockInfos[2].eq(toBN(lockAmount)));
    assert.equal(lockInfos[3].toString(), "600");
    assert(lockInfos[4].eq(lockInfos[0].add((lockInfos[1].mul(lockInfos[3])))));
    assert(lockDetails[0].eq(toBN("10")));
    assert(lockDetails[1].eq(toBN(lockAmount)));
  });

  it("should NOT lock", async () => {
    const lockAmount = toWei("100");
    // not owner
    await expectRevert(
      token.setLock(
        accounts[1],
        3600,
        10,
        lockAmount,
        600,
        { from: accounts[1] }
      ),
      "Ownable: caller is not the owner"
    );
  });

  it("should lock work properly", async () => {
    const lockAmount = toWei("100");
    await token.setLock(
      accounts[1],
      3600,
      10,
      lockAmount,
      600,
      { from: accounts[0] }
    );

    await token.transfer(accounts[1], toWei("100"), { from: accounts[0] });
    
    // it is not release time yet, so it should be reverted
    await expectRevert(
      token.transfer(accounts[2], "1", { from: accounts[1] }),
      "balance is locked"
    );

    // go to release time
    await time.increase(3601);

    let lockDetails1;
    lockDetails1 = await token.getLockDetails(accounts[1]);
    assert(lockDetails1[0].eq(toBN("9")));
    assert(lockDetails1[1].eq(toBN(lockAmount).sub(bigWei("10"))));

    // 90 ether is locked. only 10 eth
    await expectRevert(
      token.transfer(accounts[2], toWei("11"), { from: accounts[1] }),
      "balance is locked"
    );

    await token.transfer(accounts[2], toWei("10"), { from: accounts[1] });
    // now accounts[1] balance should be 90
    let balance1, balance2;
    balance1 = await token.balanceOf(accounts[1]);
    balance2 = await token.balanceOf(accounts[2]);
    assert(balance1.eq(bigWei("90")));
    assert(balance2.eq(bigWei("10")));

    await time.increase(600);

    lockDetails1 = await token.getLockDetails(accounts[1]);
    assert(lockDetails1[0].eq(toBN("8")));
    assert(lockDetails1[1].eq(toBN(lockAmount).sub(bigWei("20"))));

    await time.increase(600);

    lockDetails1 = await token.getLockDetails(accounts[1]);
    assert(lockDetails1[0].eq(toBN("7")));
    assert(lockDetails1[1].eq(toBN(lockAmount).sub(bigWei("30"))));

    await expectRevert(
      token.transfer(accounts[2], toWei("21"), { from: accounts[1] }),
      "balance is locked"
    );

    await token.transfer(accounts[2], toWei("20"), { from: accounts[1] });
    // now accounts[1] balance should be 70
    balance1 = await token.balanceOf(accounts[1]);
    balance2 = await token.balanceOf(accounts[2]);
    assert(balance1.eq(bigWei("70")));
    assert(balance2.eq(bigWei("30")));
    
    await time.increase(600 * 7);

    await token.transfer(accounts[2], toWei("70"), { from: accounts[1] });
    // now accounts[0] balance should be 0
    balance1 = await token.balanceOf(accounts[1]);
    balance2 = await token.balanceOf(accounts[2]);
    assert(balance1.eq(bigWei("0")));
    assert(balance2.eq(bigWei("100")));
  });
});