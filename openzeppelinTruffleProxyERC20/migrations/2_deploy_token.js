// migrations/2_deploy_box.js
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const Token = artifacts.require('ToUpgradeToken');

module.exports = async function (deployer) {
  const token = await deployProxy(Token, ["To Upgrade Token", "TUTT", "1000000" + "0".repeat(18)], { deployer, initializer: 'initialize' });

  console.log("Deployed at: ", token.address);
};