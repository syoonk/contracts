pragma solidity ^0.5.16;

import "./IERC20.sol";
import "./ExternStateToken.sol";
import "./TokenState.sol";

contract ToastCoin is IERC20, ExternStateToken {

  string public constant TOKEN_NAME = "Toast Coin";
  string public constant TOKEN_SYMBOL = "TSTS";
  uint8 public constant DECIMALS = 18;

  constructor(
    address payable _proxy,
    address _tokenState,
    address _owner,
    uint _totalSupply
  ) ExternStateToken(_proxy, _tokenState, TOKEN_NAME, TOKEN_SYMBOL, _totalSupply, DECIMALS, _owner) 
  public {}

  function transfer(address _to, uint _value)
  external
  optionalProxy
  returns(bool) {
    _canTransfer(messageSender, _to, _value);
    
    _transferByProxy(messageSender, _to, _value);
  }

  function transferFrom(
    address _from,
    address _to,
    uint _value
  ) external
  optionalProxy
  returns(bool) {
    _canTransfer(messageSender, _to, _value);

    return _transferFromByProxy(
      messageSender, 
      _from, 
      _to, 
      _value
    );
  }

  function _canTransfer(address _from, address _to, uint _value)
  internal {}

}
