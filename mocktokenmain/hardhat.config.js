require('dotenv').config();
require("@nomiclabs/hardhat-ethers");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const MAIN_URL = process.env.MAIN_URL;
const MAIN_PRIVATE = process.env.MAIN_PRIVATE;

const KOVAN_URL = process.env.KOVAN_URL;
const KOVAN_PRIVATE = process.env.KOVAN_PRIVATE;

module.exports = {
  solidity: "0.5.16",
  settings: {
    optimizer: true,
    runs: 200
  },

  networks: {
    mainnet: {
      url: MAIN_URL,
      accounts: [MAIN_PRIVATE],
      loggingEnabled: true,
      gasPrice: 70000000000
    },

    kovan: {
      url: KOVAN_URL,
      accounts: [KOVAN_PRIVATE],
      loggingEnabled: true
    },

    development: {
      url: "http://127.0.0.1:8545",
      accounts: [KOVAN_PRIVATE]
    }
  }
};
