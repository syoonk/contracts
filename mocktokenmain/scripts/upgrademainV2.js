const { ethers } = require("hardhat");

const main = async () => {
  const signer = await ethers.getSigner();

  console.log("The signer is... ", signer.address);
  
  const PROXY_ERC20 = "0xF0ea2baf359B597b704935c9E85b4188c3b4a84E";
  const TOKEN_STATE = "0x6Ca724e0872DE130028dfcEedFC0Db8813A44e17";
  const TOKEN = "0xEBAd6Bc6c5176D260355406403189c5555357097";
  
  const proxyERC20 = await ethers.getContractAt("ProxyERC20", PROXY_ERC20, signer);
  const tokenState = await ethers.getContractAt("TokenState", TOKEN_STATE, signer);
  const token = await ethers.getContractAt("ToastCoin", TOKEN, signer);
  
  // console.log("\nAttempting to deploy ToastCoin....");
  // const Token = await ethers.getContractFactory("ToastCoin");
  // const token = await Token.deploy(
  //   PROXY_ERC20, 
  //   TOKEN_STATE, 
  //   signer.address,
  //   "1000000000" + "0".repeat(18)
  // );
  // console.log("token is deployed at: ", token.address);

  console.log("\nSetting new ToastCoin on ProxyERC20...");
  const receipt = await proxyERC20.setTarget(token.address);
  console.dir(receipt);
  console.log("Target has been set as: ", token.address);

  console.log("\nSetting assosicated contract new ToastCoin on TokenState...");
  const receipt2 = await tokenState.setAssociatedContract(token.address);
  console.dir(receipt2);
  console.log("associatedContract has been set as: ", token.address);

  console.log("Upgrade has been done");
}

main();

/*

The signer is...  0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a

Setting new ToastCoin on ProxyERC20...
{
  hash: '0x864a09eead593c660dea0ab54458bcca10e927b487f7c0056021b1dfe5e36e21',
  type: 0,
  accessList: null,
  blockHash: null,
  blockNumber: null,
  transactionIndex: null,
  confirmations: 0,
  from: '0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a',
  gasPrice: BigNumber { _hex: '0x104c533c00', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x754d', _isBigNumber: true },
  to: '0xF0ea2baf359B597b704935c9E85b4188c3b4a84E',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  nonce: 8,
  data: '0x776d1a01000000000000000000000000ebad6bc6c5176d260355406403189c5555357097',
  r: '0xe6dd69ed96ae8c1cfcc1cb44a4951a08db2a1ef948576328be19996360e8147a',
  s: '0x65bfbb47f9ea480dc3559445527316cb7e2c326a2cd64f59db2db7d6ad40103e',
  v: 37,
  creates: null,
  chainId: 1,
  wait: [Function (anonymous)]
}
Target has been set as:  0xEBAd6Bc6c5176D260355406403189c5555357097

Setting assosicated contract new ToastCoin on TokenState...
{
  hash: '0x3c463690e4cc49229c5266ecd6465c223e345c56780c02efe10ca3721ab13a1e',
  type: 0,
  accessList: null,
  blockHash: '0xcd90670961bd830515ffb19777578ca82fa37f58f4fcfb8db6889fbc24caddbb',
  blockNumber: 12386131,
  transactionIndex: 50,
  confirmations: 1,
  from: '0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a',
  gasPrice: BigNumber { _hex: '0x104c533c00', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x754d', _isBigNumber: true },
  to: '0x6Ca724e0872DE130028dfcEedFC0Db8813A44e17',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  nonce: 9,
  data: '0x52f445ca000000000000000000000000ebad6bc6c5176d260355406403189c5555357097',
  r: '0x9df8b4b2863f8430dc3c652293e824a1d5315ff599117186330e2b1da3a583d5',
  s: '0x00054fad9a4476f9bbe3ed5bf2ef36e67872b7574962c071e75591f030307aa4',
  v: 37,
  creates: null,
  chainId: 1,
  wait: [Function (anonymous)]
}
associatedContract has been set as:  0xEBAd6Bc6c5176D260355406403189c5555357097
Upgrade has been done

*/