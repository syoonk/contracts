pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20PausableUpgradeable.sol";

contract LockToken is ERC20PausableUpgradeable {
  
  function initialize(string memory name, string memory symbol, uint256 initialSupply) 
  public virtual 
  initializer {
    __ERC20_init(name, symbol);
    __ERC20Pausable_init();
    _mint(_msgSender(), initialSupply);
  }

}