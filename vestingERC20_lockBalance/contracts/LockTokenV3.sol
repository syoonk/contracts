pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";

contract LockTokenV3 is ERC20PausableUpgradeable {
  
  using SafeMathUpgradeable for uint256;

  mapping(address => uint) private _lockAmounts;

  function initialize(string memory name, string memory symbol, uint256 initialSupply) 
  public virtual 
  initializer {
    __ERC20_init(name, symbol);
    __ERC20Pausable_init();
    _mint(_msgSender(), initialSupply);
  }

  function setLockAmount(address account, uint _lockAmount) 
  external {
    _lockAmounts[account] = _lockAmount;
  }

  function balanceOf(address account)
  public view override virtual
  returns(uint256) {
    return super.balanceOf(account);
  }

  function getLockAmount(address _account)
  external view
  returns(uint) {
    return _lockAmounts[_account];
  }

}