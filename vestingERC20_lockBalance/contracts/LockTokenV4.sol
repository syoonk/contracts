pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";

interface ITokenLocker {
  function getLock(address _account) 
  external view 
  returns (
    uint, 
    uint, 
    uint, 
    uint, 
    uint
  );

  function getLockDetails(address _account)
  external view
  returns(uint, uint);
}

contract LockTokenV4 is ERC20PausableUpgradeable {
  
  using SafeMathUpgradeable for uint256;

  bytes32 constant public TOKEN_LOCKER = "TokenLocker";

  mapping(address => uint) private _lockAmounts;

  mapping(bytes32 => address) private _contractAddresses;

  function initialize(string memory name, string memory symbol, uint256 initialSupply) 
  public virtual 
  initializer {
    __ERC20_init(name, symbol);
    __ERC20Pausable_init();
    _mint(_msgSender(), initialSupply);
  }

  function tokenLocker()
  internal view
  returns(ITokenLocker) {
    return ITokenLocker(_contractAddresses[TOKEN_LOCKER]);
  }

  function setContractAddress(bytes32 _key, address _address)
  external {
    _contractAddresses[_key] = _address;
  }

  function balanceOf(address account)
  public view override virtual
  returns(uint256) {
    ( , uint lockedAmount) = tokenLocker().getLockDetails(account);

    return super.balanceOf(account).sub(lockedAmount);
  }

  function getLock(address _account)
  external view
  returns(
    uint,
    uint,
    uint,
    uint,
    uint
  ) {
    return tokenLocker().getLock(_account);
  }

  function getContractAddress(bytes32 _key)
  external view
  returns(address) {
    return _contractAddresses[_key];
  }

}