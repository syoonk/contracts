pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";

interface ITokenLocker {
  function getLock(address _account) 
  external view 
  returns (
    uint, 
    uint, 
    uint, 
    uint, 
    uint
  );

  function getLockDetails(address _account)
  external view
  returns(uint, uint);
}

contract LockTokenV5 is ERC20PausableUpgradeable {
  
  using SafeMathUpgradeable for uint256;

  bytes32 constant public TOKEN_LOCKER = "TokenLocker";

  mapping(address => uint) private _lockAmounts;

  mapping(bytes32 => address) private _contractAddresses;

  function initialize(string memory name, string memory symbol, uint256 initialSupply) 
  public virtual 
  initializer {
    __ERC20_init(name, symbol);
    __ERC20Pausable_init();
    _mint(_msgSender(), initialSupply);
  }

  function tokenLocker()
  internal view
  returns(ITokenLocker) {
    return ITokenLocker(_contractAddresses[TOKEN_LOCKER]);
  }

  function setContractAddress(bytes32 _key, address _address)
  external {
    _contractAddresses[_key] = _address;
  }

  function balanceOf(address account)
  public view override virtual
  returns(uint256) {
    ( , uint lockedAmount) = tokenLocker().getLockDetails(account);

    return super.balanceOf(account).sub(lockedAmount);
  }

  function getLock(address _account)
  external view
  returns(
    uint,
    uint,
    uint,
    uint,
    uint
  ) {
    return tokenLocker().getLock(_account);
  }

  function getContractAddress(bytes32 _key)
  external view
  returns(address) {
    return _contractAddresses[_key];
  }

  function getIsLocked(address _account, uint _amount)
  external view
  returns(bool) {
    return _isLocked(_account, _amount);
  }

  function _isLocked(address _account, uint _amount)
  internal view
  returns(bool) {
      ( uint startTime, , , , uint endTime ) = tokenLocker().getLock(_account);

      // user is not locked
      if(startTime == 0 || endTime < block.timestamp) {
          return false;
      } 

      // releasing is not started yet
      if(startTime > block.timestamp) {
          return true;
      }

      (, uint lockAmount) = tokenLocker().getLockDetails(_account);
      uint userBalance = super.balanceOf(_account);

      // user's remained balance may less than lock amount
      if(userBalance.sub(_amount) < lockAmount) {
          return true;
      }
  }

  function _beforeTokenTransfer(address from, address to, uint256 amount) 
  internal override virtual { 
    require(
        !_isLocked(from, amount),
        "balance is locked"
    );
  }

}