const main = async () => {
  const PROXY_ADDRESS = "0x825067B5D981f6Ab2bFb27d973FfEb8e0a288Ca0";
  const TOKEN_LOCKER = "0xdc07f7c26457acd8A973AdceA76dC6c0Dc9E269C";

  const signer = await ethers.getSigner();
  console.log("The signer is ... ", signer.address);
  
  const TokenV4 = await ethers.getContractFactory("LockTokenV4");
  const upgraded = await upgrades.upgradeProxy(PROXY_ADDRESS, TokenV4);
  console.log("Token upgraded");

  console.log("Setting locker at ", TOKEN_LOCKER);
  await upgraded.setContractAddress(ethers.utils.formatBytes32String("TokenLocker"), TOKEN_LOCKER);
  console.log("locker has been set");
}

main();