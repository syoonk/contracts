const main = async () => {
  const PROXY_ADDRESS = "0x825067B5D981f6Ab2bFb27d973FfEb8e0a288Ca0";

  const signer = await ethers.getSigner();
  console.log("The signer is ... ", signer.address);

  const TokenV2 = await ethers.getContractFactory("LockTokenV2");
  const tokenV2 = await upgrades.upgradeProxy(PROXY_ADDRESS, TokenV2);
  console.log("Token upgraded");
  console.log(tokenV2);
}

main();