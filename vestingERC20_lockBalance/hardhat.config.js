require('@openzeppelin/hardhat-upgrades');
require("@nomiclabs/hardhat-ethers");

/**
 * @type import('hardhat/config').HardhatUserConfig
*/
const url = "https://kovan.infura.io/v3/8a313bcfaadd4f6384598e705151f606";
// const prv = "3833e91e92c9d57e6ac4b1f295f9a4d01145d5f29852a1363147b93db2335755";
// test main
const prv = "f7c9a8bd547b9255eca7648e81ba3b2046ab40550f74de95344a1133fc1a4684";
// test sec
// const prv = "3cce5804f04dae9bceff24a4ac9fd759299e3f18f3e7bd6e5543eb271bbb05b6";

module.exports = {
  solidity: "0.8.3",

  networks: {
    kovan: {
      url,
      accounts: [prv]
    },

    development: {
      url: "http://127.0.0.1:8545",
      accounts: [prv]
    }
  }
};