const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");

const TokenMint = artifacts.require("TokenMint");

const toBN = (_amount) => {
  return web3.utils.toBN(String(_amount));
}

const toWei = (_amount) => {
  return web3.utils.toWei(String(_amount));
}

const bigWei = (_amount) => {
  return toBN(toWei(_amount));
}

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

const TOTAL_SUPPLY = toWei(String(10**7));

contract("CappedToken", accounts => {
  let token;
  
  beforeEach(async () => {
    token = await TokenMint.new();
  });

  it("should deploy properly", async () => {
    const owner = await token.owner();

    assert.equal(owner, accounts[0]);
  });

  it("should mint", async () => {
    const balance1_before = await token.balanceOf(accounts[1]);

    const receipt = await token.mint(accounts[1], 100, {from: accounts[0]});

    const balance1_after = await token.balanceOf(accounts[1]);
    
    expectEvent(receipt, "Transfer", {from: ZERO_ADDRESS, to: accounts[1], value: "100"});
    assert.equal(Number(balance1_before), 0);
    assert.equal(Number(balance1_after), 100);
  });

  it("should NOT mint", async () => {
    // not owner 
    await expectRevert(
      token.mint(accounts[1], 100, {from: accounts[1]}),
      "Ownable: caller is not the owner"
    );

    // exceeds cap
    await expectRevert(
      token.mint(accounts[1], toBN(TOTAL_SUPPLY).add(toBN("1")), {from: accounts[0]}),
      "ERC20Capped: cap exceeded"
    );
  });

  /**
   * OWNABLE
   */
  it("should change owner", async () => {
    const owner_before = await token.owner();

    const receipt = await token.transferOwnership(accounts[9], {from: accounts[0]});
    
    const owner_after = await token.owner();

    expectEvent(receipt, "OwnershipTransferred", {previousOwner: accounts[0], newOwner: accounts[9]});
    assert.equal(owner_before, accounts[0]);
    assert.equal(owner_after, accounts[9]);
  });

  it("should NOT change owner", async () => {
    // not owner
    await expectRevert(
      token.transferOwnership(accounts[9], {from: accounts[9]}),
      "Ownable: caller is not the owner"
    );

    // new owner is empty address
    await expectRevert(
      token.transferOwnership(ZERO_ADDRESS, {from: accounts[0]}),
      "Ownable: new owner is the zero address"
    );
  });

  it("should mint with changed owner", async () => {
    await token.transferOwnership(accounts[9], {from: accounts[0]});

    await token.mint(accounts[1], 100, {from: accounts[9]});

    // should not be called by previous owner
    await expectRevert(
      token.mint(accounts[1], 100, {from: accounts[0]}),
      "Ownable: caller is not the owner"
    );
  });
});