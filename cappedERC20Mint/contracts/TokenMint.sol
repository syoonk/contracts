// SPDX-License-Indentifier: MIT

pragma solidity ^0.7.3;

import "@openzeppelin/contracts/token/ERC20/ERC20Capped.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract TokenMint is ERC20Capped, Ownable {

  uint constant TOTAL_SUPPLY = 10**7 * 10**18;

  constructor()
  Ownable()
  ERC20Capped(TOTAL_SUPPLY)
  ERC20("Mintable Capped Token", "MCT") {
  }

  function mint(address _to, uint _amount)
  external 
  onlyOwner() {
    _mint(_to, _amount);
  } 
  
}