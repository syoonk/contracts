const { ethers } = require("hardhat");

const main = async () => {
  const signer = await ethers.getSigner();

  console.log("The signer is... ", signer.address);
  
  const PROXY_ERC20 = "0xF0ea2baf359B597b704935c9E85b4188c3b4a84E";
  const TOKEN_STATE = "0x6Ca724e0872DE130028dfcEedFC0Db8813A44e17";
  
  const proxyERC20 = await ethers.getContractAt("ProxyERC20", PROXY_ERC20, signer);
  const tokenState = await ethers.getContractAt("TokenState", TOKEN_STATE, signer);
  
  console.log("\nAttempting to deploy ToastCoin....");
  const Token = await ethers.getContractFactory("ToastCoin");
  const token = await Token.deploy(
    PROXY_ERC20, 
    TOKEN_STATE, 
    signer.address,
    "1000000000" + "0".repeat(18)
  );
  console.log("token is deployed at: ", token.address);

  console.log("\nSetting new ToastCoin on ProxyERC20...");
  const receipt = await proxyERC20.setTarget(token.address);
  console.dir(receipt);
  console.log("Target has been set as: ", token.address);

  console.log("\nSetting assosicated contract new ToastCoin on TokenState...");
  const receipt2 = await tokenState.setAssociatedContract(token.address);
  console.dir(receipt2);
  console.log("associatedContract has been set as: ", token.address);

  console.log("Upgrade has been done");
}

main();

/*
syoonk@DESKTOP-CUSU7UK:~/Workspace/mocktokenmain$ npx hardhat run ./scripts/upgrademain.js --network mainnet
The signer is...  0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a

Attempting to deploy ToastCoin....
token is deployed at:  0xEBAd6Bc6c5176D260355406403189c5555357097

Setting new ToastCoin on ProxyERC20...
{
  hash: '0xf2429a27f8d007176f9fb6ac11ea0df1c2e600c96e92564772c8c4cf30068a74',
  type: 0,
  accessList: null,
  blockHash: null,
  blockNumber: null,
  transactionIndex: null,
  confirmations: 0,
  from: '0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a',
  gasPrice: BigNumber { _hex: '0x0a5f631064', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x754d', _isBigNumber: true },
  to: '0xF0ea2baf359B597b704935c9E85b4188c3b4a84E',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  nonce: 6,
  data: '0x776d1a01000000000000000000000000ebad6bc6c5176d260355406403189c5555357097',
  r: '0x01771ee510906c4f496b225236b3d14cb8ceda8af45927923218afec7d0d7ab0',
  s: '0x280357ecef0e08b2852a5d2e063f78ffff85f10cac4f1e67e89125ab4c35d7f5',
  v: 37,
  creates: null,
  chainId: 1,
  wait: [Function (anonymous)]
}
Target has been set as:  0xEBAd6Bc6c5176D260355406403189c5555357097

Setting assosicated contract new ToastCoin on TokenState...
{
  hash: '0xffe51abd79b37578baf4969f5bea363aac3c6bd169662edb76e4c9b7b10c6bc6',
  type: 0,
  accessList: null,
  blockHash: null,
  blockNumber: null,
  transactionIndex: null,
  confirmations: 0,
  from: '0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a',
  gasPrice: BigNumber { _hex: '0x0a5f631064', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x754d', _isBigNumber: true },
  to: '0x6Ca724e0872DE130028dfcEedFC0Db8813A44e17',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  nonce: 7,
  data: '0x52f445ca000000000000000000000000ebad6bc6c5176d260355406403189c5555357097',
  r: '0xf3b4a622e50e07db66865bb5066e0ef29e1838ffc088d0bae9fbcccc5d031594',
  s: '0x5d28c3e973873edf1c252586b9fe9ed801ceb7149a936d694db6d6bd8dd5279c',
  v: 38,
  creates: null,
  chainId: 1,
  wait: [Function (anonymous)]
}
associatedContract has been set as:  0xEBAd6Bc6c5176D260355406403189c5555357097
Upgrade has been done
*/