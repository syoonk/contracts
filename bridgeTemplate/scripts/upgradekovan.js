// The signer is...  0xF85dA896aa97B3Fb8c81CD0F211A04FB3fDCc26a
// Attempting to deploy ProxyERC20....
// ProxyERC20 deployed at:  0xF0ea2baf359B597b704935c9E85b4188c3b4a84E

// Attempting to deploy TokenState....
// TokenState deployed at:  0x6Ca724e0872DE130028dfcEedFC0Db8813A44e17

// Attempting to deploy Token....
// token is deployed at:  0xC056c2DeF5A8390Da1Bec90782B4475E360bf23c

// Setting TokenState associated contract as...  0xC056c2DeF5A8390Da1Bec90782B4475E360bf23c
// TokenState associated contract set as:  0xC056c2DeF5A8390Da1Bec90782B4475E360bf23c

// Setting ProxyERC20 target as...  0xC056c2DeF5A8390Da1Bec90782B4475E360bf23c
// ProxyERC20's target has been set as...  0xC056c2DeF5A8390Da1Bec90782B4475E360bf23c

// Deployment finished

/// KOVAN
/*

The signer is...  0x96C8399B3611B038513Fa2Fa8920D5870c0f2390
Attempting to deploy ProxyERC20....
ProxyERC20 deployed at:  0x2c3bbfE5Cd6D07b8E5fA2677Cc58b07076d03fb2

Attempting to deploy TokenState....
TokenState deployed at:  0x1a69ddaD67F9296509Eb976F509D257e0633c04F

Attempting to deploy Token....
token is deployed at:  0x27B88Cc26999b5916c0F36EBf244872e962cAD68

Set TokenState associated contract as...  0x27B88Cc26999b5916c0F36EBf244872e962cAD68
TokenState associated contract set as:  0x27B88Cc26999b5916c0F36EBf244872e962cAD68

Set ProxyERC20 target as...  0x27B88Cc26999b5916c0F36EBf244872e962cAD68
Set ProxyERC20 target as...  0x27B88Cc26999b5916c0F36EBf244872e962cAD68

Deployment finished

*/

const { ethers } = require("hardhat");

const main = async () => {
  const signer = await ethers.getSigner();

  console.log("The signer is... ", signer);
  
  const PROXY_ERC20 = "0x2c3bbfE5Cd6D07b8E5fA2677Cc58b07076d03fb2";
  const TOKEN_STATE = "0x1a69ddaD67F9296509Eb976F509D257e0633c04F";
  
  const proxyERC20 = await ethers.getContractAt("ProxyERC20", PROXY_ERC20, signer);
  const tokenState = await ethers.getContractAt("TokenState", TOKEN_STATE, signer);
  
  console.log("\nAttempting to deploy ToastCoin....");
  const Token = await ethers.getContractFactory("ToastCoin");
  const token = await Token.deploy(
    PROXY_ERC20, 
    TOKEN_STATE, 
    signer.address,
    "1000000000" + "0".repeat(18)
  );
  console.log("token is deployed at: ", token.address);

  console.log("\nSetting new ToastCoin on ProxyERC20...");
  const receipt = await proxyERC20.setTarget(token.address);
  console.dir(receipt);
  console.log("Target has been set as: ", token.address);

  console.log("\nSetting assosicated contract new ToastCoin on TokenState...");
  const receipt2 = await tokenState.setAssociatedContract(token.address);
  console.dir(receipt2);
  console.log("associatedContract has been set as: ", token.address);

  console.log("Upgrade has been done");
}

main();


/*
syoonk@DESKTOP-CUSU7UK:~/Workspace/mocktokenmain$ npx hardhat run ./scripts/upgradekovan.js --network kovan
The signer is...  SignerWithAddress {
  _isSigner: true,
  address: '0x96C8399B3611B038513Fa2Fa8920D5870c0f2390',
  _signer: JsonRpcSigner {
    _isSigner: true,
    provider: EthersProviderWrapper {
      _isProvider: true,
      _events: [],
      _emitted: [Object],
      formatter: [Formatter],
      anyNetwork: false,
      _networkPromise: [Promise],
      _maxInternalBlockNumber: -1024,
      _lastBlockNumber: -2,
      _pollingInterval: 4000,
      _fastQueryDate: 0,
      connection: [Object],
      _nextId: 42,
      _hardhatProvider: BackwardsCompatibilityProviderAdapter {
        _wrapped: AutomaticGasPriceProvider {
          _wrapped: AutomaticGasProvider {
            _wrapped: [AutomaticSenderProvider],
            _wrappedProvider: [AutomaticSenderProvider],
            _gasMultiplier: 1
          },
          _wrappedProvider: AutomaticGasProvider {
            _wrapped: [AutomaticSenderProvider],
            _wrappedProvider: [AutomaticSenderProvider],
            _gasMultiplier: 1
          }
        },
        _provider: AutomaticGasPriceProvider {
          _wrapped: AutomaticGasProvider {
            _wrapped: [AutomaticSenderProvider],
            _wrappedProvider: [AutomaticSenderProvider],
            _gasMultiplier: 1
          },
          _wrappedProvider: AutomaticGasProvider {
            _wrapped: [AutomaticSenderProvider],
            _wrappedProvider: [AutomaticSenderProvider],
            _gasMultiplier: 1
          }
        },
        sendAsync: [Function: bound sendAsync],
        send: [Function: bound send],
        _sendJsonRpcRequest: [Function: bound _sendJsonRpcRequest] AsyncFunction
      },
      _eventLoopCache: [Object],
      _network: [Object]
    },
    _index: 0,
    _address: null
  },
  provider: EthersProviderWrapper {
    _isProvider: true,
    _events: [],
    _emitted: { block: -2 },
    formatter: Formatter { formats: [Object] },
    anyNetwork: false,
    _networkPromise: Promise { [Object] },
    _maxInternalBlockNumber: -1024,
    _lastBlockNumber: -2,
    _pollingInterval: 4000,
    _fastQueryDate: 0,
    connection: { url: 'http://localhost:8545' },
    _nextId: 42,
    _hardhatProvider: BackwardsCompatibilityProviderAdapter {
      _wrapped: AutomaticGasPriceProvider {
        _wrapped: AutomaticGasProvider {
          _wrapped: [AutomaticSenderProvider],
          _wrappedProvider: [AutomaticSenderProvider],
          _gasMultiplier: 1
        },
        _wrappedProvider: AutomaticGasProvider {
          _wrapped: [AutomaticSenderProvider],
          _wrappedProvider: [AutomaticSenderProvider],
          _gasMultiplier: 1
        }
      },
      _provider: AutomaticGasPriceProvider {
        _wrapped: AutomaticGasProvider {
          _wrapped: [AutomaticSenderProvider],
          _wrappedProvider: [AutomaticSenderProvider],
          _gasMultiplier: 1
        },
        _wrappedProvider: AutomaticGasProvider {
          _wrapped: [AutomaticSenderProvider],
          _wrappedProvider: [AutomaticSenderProvider],
          _gasMultiplier: 1
        }
      },
      sendAsync: [Function: bound sendAsync],
      send: [Function: bound send],
      _sendJsonRpcRequest: [Function: bound _sendJsonRpcRequest] AsyncFunction
    },
    _eventLoopCache: { detectNetwork: null },
    _network: {
      name: 'kovan',
      chainId: 42,
      ensAddress: null,
      _defaultProvider: [Function]
    }
  }
}

Attempting to deploy ToastCoin....
token is deployed at:  0x723E306d83D7Fdca6CF94138b98028286ad5f0d1

Setting new ToastCoin on ProxyERC20...
{
  hash: '0xb608d09c29e24845e566b306ffca53e8cf4395619cfd6e9641114e5ccd114675',
  type: null,
  accessList: null,
  blockHash: '0xd01bf72a227de19bb60332e35347a1121bd99926034db5779453429c7f257717',
  blockNumber: 24707987,
  transactionIndex: 4,
  confirmations: 2,
  from: '0x96C8399B3611B038513Fa2Fa8920D5870c0f2390',
  gasPrice: BigNumber { _hex: '0xb2d05e00', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x7359', _isBigNumber: true },
  to: '0x2c3bbfE5Cd6D07b8E5fA2677Cc58b07076d03fb2',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  nonce: 3877,
  data: '0x776d1a01000000000000000000000000723e306d83d7fdca6cf94138b98028286ad5f0d1',
  r: '0xe669224cbf51f52dd209ab03ad038b74adbe2e67d935a711e2651ea070186d5e',
  s: '0x04ed6e9b316d460442e04c0b81128427765346ab8d797bbfc05903234fbc7817',
  v: 119,
  creates: null,
  raw: '0xf889820f2584b2d05e00827359942c3bbfe5cd6d07b8e5fa2677cc58b07076d03fb280a4776d1a01000000000000000000000000723e306d83d7fdca6cf94138b98028286ad5f0d177a0e669224cbf51f52dd209ab03ad038b74adbe2e67d935a711e2651ea070186d5ea004ed6e9b316d460442e04c0b81128427765346ab8d797bbfc05903234fbc7817',
  chainId: 42,
  wait: [Function (anonymous)]
}
Target has been set as:  0x723E306d83D7Fdca6CF94138b98028286ad5f0d1

Setting assosicated contract new ToastCoin on TokenState...
{
  hash: '0x56934287727b2a09bdf7c0c9df074a126b84c6c239cf3e0c788c1162a32abcfd',
  type: null,
  accessList: null,
  blockHash: '0x6e0ac622b10b551dbca3ec07c866267a828af462bc8b10cedad02fb907f786f6',
  blockNumber: 24707990,
  transactionIndex: 4,
  confirmations: 2,
  from: '0x96C8399B3611B038513Fa2Fa8920D5870c0f2390',
  gasPrice: BigNumber { _hex: '0xb2d05e00', _isBigNumber: true },
  gasLimit: BigNumber { _hex: '0x7359', _isBigNumber: true },
  to: '0x1a69ddaD67F9296509Eb976F509D257e0633c04F',
  value: BigNumber { _hex: '0x00', _isBigNumber: true },
  nonce: 3878,
  data: '0x52f445ca000000000000000000000000723e306d83d7fdca6cf94138b98028286ad5f0d1',
  r: '0x73e146fea78ef47f7b11a3da736bf05c81d165d5eb57553800e3865fc5460c36',
  s: '0x297b73e51f8020cd624afc20d6051d570ba415bed1d0d1f9a8b03bda8e6300a8',
  v: 120,
  creates: null,
  raw: '0xf889820f2684b2d05e00827359941a69ddad67f9296509eb976f509d257e0633c04f80a452f445ca000000000000000000000000723e306d83d7fdca6cf94138b98028286ad5f0d178a073e146fea78ef47f7b11a3da736bf05c81d165d5eb57553800e3865fc5460c36a0297b73e51f8020cd624afc20d6051d570ba415bed1d0d1f9a8b03bda8e6300a8',
  chainId: 42,
  wait: [Function (anonymous)]
}
associatedContract has been set as:  0x723E306d83D7Fdca6CF94138b98028286ad5f0d1
Upgrade has been done

*/