const { ethers } = require("hardhat");
const { 
  toBN, 
  toWei,
  bigWei, 
  fromWei, 
  ZERO_ADDRESS, 
  strToBytes32, 
  timeUnit,
  toUnit,
  fromUnit
} = require("./helpers.js");

const main = async () => {
  const signer = await ethers.getSigner();
  console.log("The signer is... ", signer.address);

  console.log("Attempting to deploy ProxyERC20....");
  const ProxyERC20 = await ethers.getContractFactory("ProxyERC20");
  const proxyERC20 = await ProxyERC20.deploy(signer.address);
  console.log("ProxyERC20 deployed at: ", proxyERC20.address);

  console.log("\nAttempting to deploy TokenState....");
  const TokenState = await ethers.getContractFactory("TokenState");
  const tokenState = await TokenState.deploy(signer.address, signer.address);
  console.log("TokenState deployed at: ", tokenState.address);

  console.log("\nAttempting to deploy BridgeState....");
  const BridgeState = await ethers.getContractFactory("BridgeState");
  const bridgeState = await BridgeState.deploy(signer.address, signer.address);
  console.log("BridgeState deployed at: ", bridgeState.address);

  console.log("\nAttempting to deploy Token....");
  const Token = await ethers.getContractFactory("Seul");
  const token = await Token.deploy(
    proxyERC20.address, 
    tokenState.address, 
    signer.address,
    bridgeState.address,
    "1000000000" + "0".repeat(18)
  );
  console.log("token is deployed at: ", token.address);

  console.log("\nSetting TokenState associated contract as... ", token.address);
  await tokenState.setAssociatedContract(token.address);
  console.log("TokenState associated contract set as: ", token.address);

  console.log("\nSetting ProxyERC20 target as... ", token.address);
  await proxyERC20.setTarget(token.address);
  console.log("ProxyERC20's target has been set as... ", token.address);
  
  console.log("\nSetting BridgeState associated contract as... ", token.address);
  await bridgeState.setAssociatedContract(token.address);
  console.log("BridgeState associated contract set as: ", token.address);

  console.log("\nSetting validator role to... ", signer.address);
  await bridgeState.setRole(strToBytes32("Validator"), signer.address, true);
  console.log("Validator set as: ", signer.address);

  console.log("\nSetting tester role to... ", signer.address);
  await bridgeState.setRole(strToBytes32("Tester"), signer.address, true);
  console.log("Tester set as: ", signer.address);

  console.log("\nOpen mumbai... ", 80001);
  await bridgeState.setNetworkStatus(80001, true);
  console.log(`Chain ID ${80001} is opened`);
  
  console.log("\nDeployment finished");
}

main();