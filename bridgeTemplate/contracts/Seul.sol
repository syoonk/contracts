pragma solidity ^0.5.16;

import "./IERC20.sol";
import "./ExternStateToken.sol";
import "./TokenState.sol";
import "./interfaces/IBridgeState.sol";

import "hardhat/console.sol";

contract Seul is IERC20, ExternStateToken {

  string public constant TOKEN_NAME = "Seul Token";
  string public constant TOKEN_SYMBOL = "SEUL";
  uint8 public constant DECIMALS = 18;

  IBridgeState public bridgeState;

  constructor(
    address payable _proxy,
    address _tokenState,
    address _owner,
    address _bridgeState,
    uint _totalSupply
  ) ExternStateToken(_proxy, _tokenState, TOKEN_NAME, TOKEN_SYMBOL, _totalSupply, DECIMALS, _owner) 
  public {
    bridgeState = IBridgeState(_bridgeState);
  }

  function setBridgeState(address _newBridgeState)
  external {
    bridgeState = IBridgeState(_newBridgeState);
  }

  function transfer(address _to, uint _value)
  external
  optionalProxy
  returns(bool) {
    _canTransfer(messageSender, _to, _value);
    
    _transferByProxy(messageSender, _to, _value);
  }

  function transferFrom(
    address _from,
    address _to,
    uint _value
  ) external
  optionalProxy
  returns(bool) {
    _canTransfer(messageSender, _to, _value);

    return _transferFromByProxy(
      messageSender, 
      _from, 
      _to, 
      _value
    );
  }

  function overchainTransfer(uint _amount, uint _destChainId)
  external 
  optionalProxy
  onlyTester {
    require(_amount > 0,
      "Cannot transfer zero");

    require(_burn(messageSender, _amount), "burning failed");

    bridgeState.appendOutboundingRequest(messageSender, _amount, _destChainId);
  }

  function claimAllBridgedAmounts()
  external 
  optionalProxy 
  onlyTester {
    uint[] memory applicableIds = bridgeState.applicableInboundIds(messageSender);

    require(applicableIds.length > 0,
      "No claimable");

    for(uint i = 0; i < applicableIds.length; i++) {
      _claimBridgedAmount(applicableIds[i]);
    }
  }

  function claimBridgedAmount(uint _index) 
  external 
  optionalProxy 
  onlyTester {
    _claimBridgedAmount(_index);
  }

  function mint(address _target, uint _amount)
  external {
    _mint(_target, _amount);
  }

  function _mint(address _target, uint _amount)
  internal 
  returns(bool) {
    tokenState.setBalanceOf(_target, tokenState.balanceOf(_target).add(_amount));

    emitTransfer(address(0), _target, _amount);    

    return true;
  }

  function _burn(address _target, uint _amount)
  internal
  returns(bool) {
    tokenState.setBalanceOf(_target, tokenState.balanceOf(_target).sub(_amount));

    emitTransfer(_target, address(0), _amount);

    return true;
  }

  function _claimBridgedAmount(uint _index)
  internal 
  returns(bool) {
    // Validations are checked from bridge state 
    (address account, uint amount, , , ) = bridgeState.inboundings(_index);

    require(account == messageSender,
      "Caller is not matched");

    bridgeState.claimInbound(_index);

    require(_mint(account, amount), "Mint failed");

    return true;
  }

  modifier onlyTester() {
    bytes32 tester = "Tester";
    
    require(bridgeState.isOnRole(tester, messageSender),
      "Not tester");
    _;
  }

  function _canTransfer(address _from, address _to, uint _value)
  internal {}

}