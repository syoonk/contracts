require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomiclabs/hardhat-truffle5");
require("@nomiclabs/hardhat-web3");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const MAIN_URL = process.env.MAIN_URL;
const MAIN_PRIVATE = process.env.MAIN_PRIVATE;

const KOVAN_URL = process.env.KOVAN_URL;
const KOVAN_PRIVATE = process.env.KOVAN_PRIVATE;

const GOERLI_URL = process.env.GOERLI_URL;
const GOERLI_PRIVATE = process.env.GOERLI_PRIVATE;

module.exports = {
  solidity: "0.5.16",
  settings: {
    optimizer: true,
    runs: 200
  },

  networks: {
    mainnet: {
      url: MAIN_URL,
      accounts: [MAIN_PRIVATE],
      loggingEnabled: true,
      gasPrice: 70000000000
    },

    kovan: {
      url: KOVAN_URL,
      accounts: [KOVAN_PRIVATE],
      loggingEnabled: true
    },

    goerli: {
      url: GOERLI_URL,
      accounts: [GOERLI_PRIVATE],
      loggingEnabled: true,
      gasPrice: 50000000000
    },

    development: {
      url: "http://127.0.0.1:8545",
      accounts: [KOVAN_PRIVATE]
    }
  }
};
