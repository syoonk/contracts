const { expectRevert, expectEvent, time } = require("@openzeppelin/test-helpers");
const { assertion } = require("@openzeppelin/test-helpers/src/expectRevert");

const ProxyERC20 = artifacts.require("ProxyERC20");
const TokenState = artifacts.require("TokenState");
const Seul = artifacts.require("Seul");
const BridgeState = artifacts.require("BridgeState");

const { 
  toBN, 
  toWei,
  bigWei, 
  fromWei, 
  ZERO_ADDRESS, 
  strToBytes32, 
  timeUnit,
  toUnit,
  fromUnit
} = require("../scripts/helpers.js");

contract("Bridge Tester", accounts => {
  const owner = accounts[0];
  const validator = accounts[9];
  const tester = accounts[1];

  // ChainId 0, 1
  let proxyERC20, tokenState, seulToken, bridgeState, seul;

  beforeEach(async () => {
    // ChainId 0
    proxyERC20 = await ProxyERC20.new(owner);
    tokenState = await TokenState.new(owner, owner);
    bridgeState = await BridgeState.new(owner, owner);
    seulToken = await Seul.new(
      proxyERC20.address,
      tokenState.address,
      owner,
      bridgeState.address,
      toUnit("100000000")
    );

    await tokenState.setAssociatedContract(seulToken.address, { from: owner });
    await proxyERC20.setTarget(seulToken.address, { from: owner });
    await bridgeState.setAssociatedContract(seulToken.address, { from: owner });
    await bridgeState.setRole(strToBytes32("Validator"), validator, true, { from: owner });
    await bridgeState.setRole(strToBytes32("Tester"), tester, true, { from: owner });
    await bridgeState.setNetworkStatus(97, true, { from: owner });

    seul = await Seul.at(proxyERC20.address);
  });

  describe("Deployments", () => {
    it("should deploy and set", async () => {
      const bridge = await seul.bridgeState();
      const associatedContract = await bridgeState.associatedContract();
      const isValidator = await bridgeState.isOnRole(strToBytes32("Validator"), validator);
      const isTester = await bridgeState.isOnRole(strToBytes32("Tester"), tester);

      assert.equal(bridge, bridgeState.address);
      assert.equal(associatedContract, seulToken.address);
      assert.equal(isValidator, true);
      assert.equal(isTester, true);
    });
  });

  describe("outbounding", () => {

    beforeEach(async () => {
      await seul.transfer(tester, toUnit("10000"), { from: owner });

      const testerBalance = await seul.balanceOf(tester);
      assert.equal(testerBalance.toString(), toUnit("10000"));
    });
    
    it("should burn and append outbounding", async () => {
      const balance_before_tester = await seul.balanceOf(tester);

      const receipt_overchainTransfer = await seul.overchainTransfer(toUnit("1"), 97, { from: tester });
      
      const balance_after_tester = await seul.balanceOf(tester);

      expectEvent(receipt_overchainTransfer, "Transfer", { from: tester, to: ZERO_ADDRESS, value: toUnit("1") });

      assert(balance_after_tester.add(toBN(toUnit("1"))).eq(balance_before_tester));

      const outbounding = await bridgeState.outboundings(0);
      const accountOutboundings = await bridgeState.accountOutboundingsInPeriod(tester, 0);
      const outboundIdsInPeriod = await bridgeState.outboundIdsInPeriod(0);
      const outboundingLength = await bridgeState.outboundingsLength();;

      assert.equal(outbounding.account, tester);
      assert.equal(outbounding.amount.toString(), toUnit("1"));
      assert.equal(outbounding.destChainId.toNumber(), 97);
      assert.equal(outbounding.periodId.toNumber(), 0);
      assert.equal(accountOutboundings.length, 1);
      assert.equal(accountOutboundings[0].toNumber(), 0);
      assert.equal(outboundIdsInPeriod.length, 1);
      assert.equal(outboundIdsInPeriod[0].toNumber(), 0);
      assert.equal(outboundingLength, 1);
    });

    it("should NOT transfer to overchain", async () => {
      // not tester
      await expectRevert(
        seul.overchainTransfer(toUnit("1"), 97, { from: owner }),
        "Not tester"
      ); 

      // not enough balance
      await expectRevert(
        seul.overchainTransfer(toUnit("10001"), 97, { from: tester }),
        "SafeMath: subtraction overflow"
      );
    });
    
  });
  
  describe("inbounding claim", () => {
    
    beforeEach(async () => {
      await bridgeState.appendMultipleInboundingRequests(
        [tester, tester],
        [toUnit("10"), toUnit("100")],
        [97, 97],
        [0, 1],
        { from: validator }
      );
    });
    
    it("should claim all", async () => {
      const balance_before_tester = await seul.balanceOf(tester);

      const receipt_claimAll = await seul.claimAllBridgedAmounts({ from: tester });
      
      const balance_after_tester = await seul.balanceOf(tester);

      expectEvent(receipt_claimAll, "Transfer", { from: ZERO_ADDRESS, to: tester, value: toUnit("100") });
      expectEvent(receipt_claimAll, "Transfer", { from: ZERO_ADDRESS, to: tester, value: toUnit("10") });
      // in the raw logs
      // expectEvent(receipt_claimAll, "InboundingClaimed", { inboundId: "0"} );
      // expectEvent(receipt_claimAll, "InboundingClaimed", { inboundId: "1"} );

      assert(balance_after_tester.sub(toBN(toUnit("110"))).eq(balance_before_tester));
    });

    it("should claim each", async () => {
      const balance_before_tester = await seul.balanceOf(tester);

      await seul.claimBridgedAmount(0, { from: tester });

      const balance_after_tester = await seul.balanceOf(tester);
      const applicableInboundIds = await bridgeState.applicableInboundIds(tester);

      assert(balance_after_tester.sub(toBN(toUnit("10"))).eq(balance_before_tester));
      assert.equal(applicableInboundIds.length, 1);
      assert.equal(applicableInboundIds[0].toNumber(), 1);
    });

    it("should NOT claim", async () => {      
      await expectRevert(
        seul.claimBridgedAmount(0, { from: owner }),
        "Not tester"
      );

      await seul.claimAllBridgedAmounts({ from: tester });

      // no claimable
      await expectRevert(
        seul.claimAllBridgedAmounts({ from: tester }),
        "No claimable"
      );
    });
    
  });
  
});