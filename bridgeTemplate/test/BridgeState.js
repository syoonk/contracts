const { expectRevert, expectEvent, time } = require("@openzeppelin/test-helpers");
const { assertion } = require("@openzeppelin/test-helpers/src/expectRevert");

const BridgeState = artifacts.require("BridgeState");

const { 
  toBN, 
  toWei,
  bigWei, 
  fromWei, 
  ZERO_ADDRESS, 
  strToBytes32, 
  timeUnit,
  toUnit,
  fromUnit
} = require("../scripts/helpers.js");

contract("Bridge State", accounts => {
  const owner = accounts[0];
  const validator = accounts[9];
  const token = accounts[10];
  const users = [
    accounts[1],
    accounts[2],
    accounts[3],
    accounts[4],
    accounts[5],
    accounts[6],
    accounts[7],
    accounts[8]
  ];

  let bridgeState;
  
  beforeEach(async () => {
    bridgeState = await BridgeState.new(owner, token);

    await bridgeState.setRole(strToBytes32("Validator"), validator, true, { from: owner });
    await bridgeState.setNetworkStatus(42, true, { from: owner });
    await bridgeState.setNetworkStatus(97, true, { from: owner });
  });

  describe("Deployment", () => {
    
    it("should set role", async () => {
      const result_validator_validator = await bridgeState.isOnRole(strToBytes32("Validator"), validator);
      const result_validator_owner = await bridgeState.isOnRole(strToBytes32("Validator"), owner);

      assert.equal(result_validator_validator, true);
      assert.equal(result_validator_owner, false);
    });

  });

  describe("Outbounding", () => {
    
    it("should request outbounding", async () => {
      const receipt = await bridgeState.appendOutboundingRequest(users[0], toUnit("1"), 97, { from: token });
      const outbound_0 = await bridgeState.outboundings(0);
      const outboundingLength = await bridgeState.outboundingsLength();
      const currentPeriodId = await bridgeState.currentOutboundPeriodId();
      const accountOutboundings_0 = await bridgeState.outboundRequestIdsInPeriod(users[0], 0);

      expectEvent(receipt, "OutboundingAppended", {
        from: users[0],
        amount: toUnit("1"),
        destChainId: "97",
        outboundId: outboundingLength.sub(toBN("1")).toString(),
        periodId: currentPeriodId.toString()
      });

      assert.equal(outbound_0[0], users[0]);
      assert(outbound_0[1].eq(toUnit("1")));
      assert.equal(outbound_0[2].toString(), "97");
      assert(outbound_0[3].eq(currentPeriodId));
      assert.equal(accountOutboundings_0.length, 1);
      assert.equal(accountOutboundings_0[0].toString(), "0");
    });

    it("should NOT request outbounding", async () => {
      // Not associated contract
      await expectRevert(
        bridgeState.appendOutboundingRequest(users[0], toUnit("1"), 97, { from: owner }),
        "Only the associated contract can perform this action"
      );

      // Not supported network
      await expectRevert(
        bridgeState.appendOutboundingRequest(users[0], toUnit("1"), 0, { from: token }),
        "Invalid target network"
      );
    });

    it("should close if list reaches number limit", async () => {
      let _accounts = [];

      for(let i = 0; i < 11; i++) {
        _accounts.push(web3.eth.accounts.create(String(i)));
      }

      for(let i = 0; i < 9; i++) {
        await bridgeState.appendOutboundingRequest(_accounts[i].address, toUnit("1"), 97, { from: token });
      }

      // 10th outbounding
      const receipt_newPeriod = await bridgeState.appendOutboundingRequest(_accounts[9].address, toUnit("2"), 97, { from: token });
      expectEvent(receipt_newPeriod, "PeriodClosed", { periodId: "0" });

      const period_0 = await bridgeState.outboundPeriods(0);
      const period_0_outboundingIds = await bridgeState.outboundIdsInPeriod(0);
      assert.equal(period_0["periodId"].toString(), "0");
      assert.equal(period_0["processed"], false);
      assert.equal(period_0_outboundingIds.length, 10);

      const period_1 = await bridgeState.outboundPeriods(1);
      const period_1_outboundingIds = await bridgeState.outboundIdsInPeriod(1);
      assert.equal(period_1["periodId"].toString(), "1");
      assert.equal(period_1["processed"], false);
      assert.equal(period_1_outboundingIds.length, 0);

      // 11th outbounding
      const receipt_11thOutbounding = await bridgeState.appendOutboundingRequest(_accounts[10].address, toUnit("3"), 97, { from: token });
      expectEvent(receipt_11thOutbounding, "OutboundingAppended", {
        from: _accounts[10].address,
        amount: toUnit("3"),
        destChainId: "97",
        outboundId: "10", 
        periodId: "1"
      });

      const currentPeriodId = await bridgeState.currentOutboundPeriodId();
      const accountOutboundings_10 = await bridgeState.outboundRequestIdsInPeriod(_accounts[10].address, 1);
      assert.equal(currentPeriodId.toString(), "1");
      assert.equal(accountOutboundings_10.length, 1);
      assert.equal(accountOutboundings_10[0].toString(), "10");
    });

    it("should close if period reaches time duration", async () => {
      for(let i = 0; i < 3; i++) {
        await bridgeState.appendOutboundingRequest(users[i], toUnit("1"), 97, { from: token });
      }

      await time.increase(601);
      
      const receipt_newPeriod = await bridgeState.appendOutboundingRequest(users[3], toUnit("3"), 97, { from: token });

      expectEvent(receipt_newPeriod, "PeriodClosed", { periodId: "0" });

      const currentPeriodId = await bridgeState.currentOutboundPeriodId();
      assert.equal(currentPeriodId.toString(), "1");
    });

    describe("Outbound process", () => {

      beforeEach(async () => {
        let _accounts = [];

        for(let i = 0; i < 35; i++) {
          _accounts.push(web3.eth.accounts.create(String(i)));
        }

        await Promise.all(_accounts.map((_account, _idx) => bridgeState.appendOutboundingRequest(_account.address, toUnit(String(_idx)), 97, { from: token })));

        const currentPeriodId = await bridgeState.currentOutboundPeriodId();

        assert.equal(currentPeriodId.toString(), "3");

        const idsToProcess = await bridgeState.periodIdsToProcess();

        assert.equal(idsToProcess.length, 3);
      });

      it("should process outbound", async () => {
        const receipt_process_1 = await bridgeState.processOutboundPeriod(1, { from: validator });

        expectEvent(receipt_process_1, "PeriodProcessed", { periodId: "1" });

        const idsToProcess = await bridgeState.periodIdsToProcess();

        assert.equal(idsToProcess.length, 2);
        assert.equal(idsToProcess[0].toString(), "0");
        assert.equal(idsToProcess[1].toString(), "2");

        const outboundPeriod_1 = await bridgeState.outboundPeriods(1);

        assert.equal(outboundPeriod_1["processed"], true);
      });

      it("should NOT process outbound", async () => {
        // not validator
        await expectRevert(
          bridgeState.processOutboundPeriod(0, { from: owner }),
          "Caller is not validator"
        );

        // not on the list
        await expectRevert(
          bridgeState.processOutboundPeriod(3, { from: validator }),
          "Period is not on the process list"
        );
        await expectRevert(
          bridgeState.processOutboundPeriod(4, { from: validator }),
          "This period id is not started yet"
        );

        // already processed
        await bridgeState.processOutboundPeriod(1, { from: validator });
        await expectRevert(
          bridgeState.processOutboundPeriod(1, { from: validator }),
          "Period is not on the process list"
        );
      });
    });
    
  });

  describe("Inbounding", () => {

    let bridgeState_97, bridgeState_80001;
    beforeEach(async () => {
      bridgeState_97 = await BridgeState.new(owner, token);
      bridgeState_80001 = await BridgeState.new(owner, token);

      await bridgeState_97.setRole(strToBytes32("Validator"), validator, true, { from: owner });
      await bridgeState_97.setNetworkStatus(42, true, { from: owner });
      await bridgeState_97.setNetworkStatus(97, true, { from: owner });
      await bridgeState_97.setNetworkStatus(80001, true, { from: owner });
      await bridgeState_80001.setRole(strToBytes32("Validator"), validator, true, { from: owner });
      await bridgeState_80001.setNetworkStatus(42, true, { from: owner });
      await bridgeState_80001.setNetworkStatus(97, true, { from: owner });
      await bridgeState_80001.setNetworkStatus(80001, true, { from: owner });
      await bridgeState.setNetworkStatus(80001, true, { from: owner });
    });
    
    it("should inbound", async () => {
      // Move from 42 to other networks
      const outbounds = [
        { // Id: 0
          user: users[0],
          amount: toUnit("97"),
          destChainId: 97
        },
        { // Id: 1
          user: users[1],
          amount: toUnit("80001"),
          destChainId: 80001
        },
        { // Id: 2
          user: users[2],
          amount: toUnit("800012"),
          destChainId: 80001
        },
        { // Id: 3
          user: users[2],
          amount: toUnit("973"),
          destChainId: 97
        },
        { // Id: 4
          user: users[1],
          amount: toUnit("800014"),
          destChainId: 80001
        }
      ];

      for(let i = 0; i < outbounds.length; i++) {
        await bridgeState.appendOutboundingRequest(
          outbounds[i].user, 
          outbounds[i].amount, 
          outbounds[i].destChainId, 
          { from: token }
        );
      }

      await bridgeState.closeOutboundPeriod({ from: validator });

      const outbounds_0 = await bridgeState.outboundIdsInPeriod(0);
      
      let inputs = { 
        97: {
          accounts: [],
          amounts: [],
          srcChainIds: [],
          srcOutboundingIds: []
        },
        80001: {
          accounts: [],
          amounts: [],
          srcChainIds: [],
          srcOutboundingIds: []          
        }
      }

      for(let outbound of outbounds_0) {
        const outbounding = await bridgeState.outboundings(outbound);
        const toChain = outbounding["destChainId"].toNumber();
        inputs[toChain].accounts.push(outbounding.account);
        inputs[toChain].amounts.push(outbounding.amount);
        inputs[toChain].srcChainIds.push(42);
        inputs[toChain].srcOutboundingIds.push(outbound);
      }

      const receipt_97 = await bridgeState_97.appendMultipleInboundingRequests(
        inputs[97].accounts,
        inputs[97].amounts,
        inputs[97].srcChainIds,
        inputs[97].srcOutboundingIds,
        { from: validator }
      );
      const receipt_80001 = await bridgeState_80001.appendMultipleInboundingRequests(
        inputs[80001].accounts,
        inputs[80001].amounts,
        inputs[80001].srcChainIds,
        inputs[80001].srcOutboundingIds,
        { from: validator }
      );

      expectEvent(receipt_97, "InboundingAppended", {
        from: users[0],
        amount: toUnit("97"),
        srcChainId: "42",
        srcOutboundingId: "0",
        inboundId: "0"
      });
      expectEvent(receipt_97, "InboundingAppended", {
        from: users[2],
        amount: toUnit("973"),
        srcChainId: "42",
        srcOutboundingId: "3",
        inboundId: "1"
      });
      expectEvent(receipt_80001, "InboundingAppended", {
        from: users[1],
        amount: toUnit("80001"),
        srcChainId: "42",
        srcOutboundingId: "1",
        inboundId: "0"
      });
      expectEvent(receipt_80001, "InboundingAppended", {
        from: users[2],
        amount: toUnit("800012"),
        srcChainId: "42",
        srcOutboundingId: "2",
        inboundId: "1"
      });
      expectEvent(receipt_80001, "InboundingAppended", {
        from: users[1],
        amount: toUnit("800014"),
        srcChainId: "42",
        srcOutboundingId: "4",
        inboundId: "2"
      });

      const inboundingLength_97 = await bridgeState_97.inboundingsLength();
      const inboundingLength_80001 = await bridgeState_80001.inboundingsLength();
      const userInboundings_97 = await Promise.all(users.map(_user => bridgeState_97.applicableInboundIds(_user)));
      const userInboundings_80001 = await Promise.all(users.map(_user => bridgeState_80001.applicableInboundIds(_user)));

      assert.equal(inboundingLength_97.toNumber(), 2);
      assert.equal(inboundingLength_80001.toNumber(), 3);
      assert.equal(userInboundings_97[0].length, 1);
      assert.equal(userInboundings_97[1].length, 0);
      assert.equal(userInboundings_97[2].length, 1);
      assert.equal(userInboundings_80001[0].length, 0);
      assert.equal(userInboundings_80001[1].length, 2);
      assert.equal(userInboundings_80001[2].length, 1);

      const srcOutboundingIdRegistered_97_0 = await bridgeState_97.srcOutboundingIdRegistered(42, 0);
      const srcOutboundingIdRegistered_97_3 = await bridgeState_97.srcOutboundingIdRegistered(42, 3);
      const srcOutboundingIdRegistered_80001_1 = await bridgeState_80001.srcOutboundingIdRegistered(42, 1);
      const srcOutboundingIdRegistered_80001_2 = await bridgeState_80001.srcOutboundingIdRegistered(42, 2);
      const srcOutboundingIdRegistered_80001_4 = await bridgeState_80001.srcOutboundingIdRegistered(42, 4);
      
      assert.equal(srcOutboundingIdRegistered_97_0, true);
      assert.equal(srcOutboundingIdRegistered_97_3, true);
      assert.equal(srcOutboundingIdRegistered_80001_1, true);
      assert.equal(srcOutboundingIdRegistered_80001_2, true);
      assert.equal(srcOutboundingIdRegistered_80001_4, true);
    });

    it("should NOT inbound", async () => {
      // not validator
      await expectRevert(
        bridgeState.appendInboundingRequest(
          users[0], 
          toUnit("1"), 
          42, 
          20,
          { from: owner }),
        "Caller is not validator"
      );
      await expectRevert(
        bridgeState.appendMultipleInboundingRequests(
          [users[0], users[1]], 
          [toUnit("1"), toUnit("2")],
          [42, 97],
          [20, 21],
          { from: owner }  
        ),
        "Caller is not validator"
      );
    
      // length is not matched
      await expectRevert(
        bridgeState.appendMultipleInboundingRequests(
          [], 
          [toUnit("1"), toUnit("2")],
          [42, 97],
          [20, 21],
          { from: validator }  
        ),
        "Input length is invalid"
      );
      await expectRevert(
        bridgeState.appendMultipleInboundingRequests(
          [users[0], users[1]], 
          [toUnit("1")],
          [42, 97],
          [20, 21],
          { from: validator }  
        ),
        "Input length is not matched"
      );

      // registered
      await bridgeState.appendInboundingRequest(
        users[0], 
        toUnit("1"),
        42,
        50,
        { from: validator }  
      );

      await expectRevert(
        bridgeState.appendInboundingRequest(
          users[0], 
          toUnit("1"),
          42,
          50,
          { from: validator }  
        ),
        "Request id is already registered to inbound"
      );
      await expectRevert(
        bridgeState.appendMultipleInboundingRequests(
          [users[0], users[1]], 
          [toUnit("1"), toUnit("2")],
          [42, 42], 
          [20, 20],
          { from: validator }  
        ),
        "Request id is already registered to inbound"
      );
    });
    
  });

  describe("claim", async () => {
    
    beforeEach(async () => {
      await bridgeState.appendMultipleInboundingRequests(
        [users[0], users[1], users[0]],
        [toUnit("1"), toUnit("2"), toUnit("3")],
        [42, 42, 97],
        [0, 1, 0],
        { from: validator }
      );
    });
    
    it("should claim", async () => {
      const receipt_0 = await bridgeState.claimInbound(0, { from: token });
      const receipt_1 = await bridgeState.claimInbound(1, { from: token });
      const receipt_2 = await bridgeState.claimInbound(2, { from: token });

      expectEvent(receipt_0, "InboundingClaimed", { inboundId: "0" });
      expectEvent(receipt_1, "InboundingClaimed", { inboundId: "1" });
      expectEvent(receipt_2, "InboundingClaimed", { inboundId: "2" });

      const accountInbounds_0 = await bridgeState.applicableInboundIds(users[0]);
      const accountInbounds_1 = await bridgeState.applicableInboundIds(users[1]);
      
      assert.equal(accountInbounds_0.length, 0);
      assert.equal(accountInbounds_1.length, 0);

      const inbounding_0 = await bridgeState.inboundings(0);
      const inbounding_1 = await bridgeState.inboundings(1);
      const inbounding_2 = await bridgeState.inboundings(2);

      assert.equal(inbounding_0.claimed, true);
      assert.equal(inbounding_1.claimed, true);
      assert.equal(inbounding_2.claimed, true);
    });

    it("should NOT claim", async () => {
      // not associated contract
      await expectRevert(
        bridgeState.claimInbound(0, { from: owner }),
        "Only the associated contract can perform this action"
      );

      // already claimed
      await bridgeState.claimInbound(1, { from: token });

      await expectRevert(
        bridgeState.claimInbound(1, { from: token }),
        "This inbounding has already been claimed"
      );
    });
    
  });

});