const { expectRevert, expectEvent, time } = require("@openzeppelin/test-helpers");
const { assertion } = require("@openzeppelin/test-helpers/src/expectRevert");

const ProxyERC20 = artifacts.require("ProxyERC20");
const TokenState = artifacts.require("TokenState");
const SeulToken = artifacts.require("SeulToken");

const { 
  toBN, 
  toWei,
  bigWei, 
  fromWei, 
  ZERO_ADDRESS, 
  strToBytes32, 
  timeUnit 
} = require("../scripts/helpers.js");

contract("Mapper Methods", (accounts) => {

  let proxyERC20, tokenState, seulToken;
  const owner = accounts[0];
  const predicate = accounts[9];

  beforeEach(async () => {
    proxyERC20 = await ProxyERC20.new(owner);
    tokenState = await TokenState.new(owner, owner);
    seulToken = await SeulToken.new(
      proxyERC20.address,
      tokenState.address,
      owner,
      "1000000000" + "0".repeat(18),
      predicate
    );

    await tokenState.setAssociatedContract(seulToken.address, { from: owner });
    await proxyERC20.setTarget(seulToken.address, { from: owner });
  });

  describe("Deploy", () => {

    it("should deployed correctly", async () => {
      const associatedContract_tokenState = await tokenState.associatedContract();
      const target_proxy = await proxyERC20.target();
      const proxy_seulToken = await seulToken.proxy();
      const tokenState_seulToken = await seulToken.tokenState();
      const predicate_seulToken = await seulToken.predicateProxy();
  
      assert.equal(associatedContract_tokenState, seulToken.address);
      assert.equal(target_proxy, seulToken.address);
      assert.equal(proxy_seulToken, proxyERC20.address);
      assert.equal(tokenState_seulToken, tokenState.address);
      assert.equal(predicate_seulToken, predicate);
    });
  
  });

  describe("mint", async () => {
    
    let seulProxy;
    
    beforeEach(async () => {
      seulProxy = await SeulToken.at(proxyERC20.address);
    });

    describe("Deposit", () => {

      it("should mint", async () => {
        await seulProxy.mint(accounts[3], toWei("100"), { from: predicate });
  
        const balance_3_after = await seulProxy.balanceOf(accounts[3]);
  
        assert(balance_3_after.eq(bigWei('100')));
      });
  
      it("should NOT mint", async () => {
        // not manager
        await expectRevert(
          seulProxy.mint(accounts[3], toWei("100"), { from: owner }),
          "Caller is not allowed to mint"
        );
      });

    });

  });
  
});