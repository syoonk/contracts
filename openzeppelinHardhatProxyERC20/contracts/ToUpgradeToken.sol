pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";

contract ToUpgradeToken is ERC20Upgradeable {

  function initialize(string memory name, string memory symbol, uint256 initialSupply) 
  public virtual 
  initializer {
    __ERC20_init(name, symbol);
    _mint(_msgSender(), initialSupply);
  }

  function transferDouble(address _to, uint _amount)
  external {
    _transfer(msg.sender, _to, _amount * 2);
  }
  
}