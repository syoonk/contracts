const main = async () => {
  const signer = await ethers.getSigner();
  
  console.log("The signer is ... ", signer.address);
  
  const Token = await ethers.getContractFactory("ToUpgradeToken");
  const token = await upgrades.deployProxy(Token, ["To Upgrade Token", "TUT", "1000000" + "0".repeat(18)], { initializer: 'initialize' });
  await token.deployed();
  console.log("Token deployed to:", token.address);

  // const receipt = await token.transferProxyAdminOwnership(signer.address);
  await upgrades.admin.transferProxyAdminOwnership(signer.address);
}

main();