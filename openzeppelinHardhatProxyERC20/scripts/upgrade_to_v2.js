const main = async () => {
  const PROXY_ADDRESS = "0x3DE71b071Cf579652c7F78932ecC4C709a5e0D3c";

  const TokenV2 = await ethers.getContractFactory("ToUpgradeTokenV2");
  const tokenV2 = await upgrades.upgradeProxy(PROXY_ADDRESS, TokenV2);
  console.log("Token upgraded");
  console.log(tokenV2);
}

main();