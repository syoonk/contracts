const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");

const Token = artifacts.require("PrivilegeToken");

const toBN = (_amount) => {
  return web3.utils.toBN(String(_amount));
}

const toWei = (_amount) => {
  return web3.utils.toWei(String(_amount));
}

const bigWei = (_amount) => {
  return toBN(toWei(_amount));
}

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

contract("Privilege Token", accounts => {
  let token;
  
  beforeEach(async () => {
    token = await Token.new();
  });

  // ISSUER PART
  it("should deploy properly", async () => {
    const issuer = await token.issuer();
    const nextTokenId = await token.nextTokenId();
    const name = await token.name();
    const symbol = await token.symbol();

    assert.equal(issuer, accounts[0]);
    assert.equal(nextTokenId.toNumber(), 0);
    assert.equal(name, "Privilege Token");
    assert.equal(symbol, "PNFT");
  });

  it("should transfer owner", async () => {
    const receipt = await token.changeIssuer(accounts[1], {from: accounts[0]});
    const newIssuer = await token.issuer();

    expectEvent(receipt, "IssuerTransferred", {previousIssuer: accounts[0], newIssuer: accounts[1]});
    assert.equal(newIssuer, accounts[1]);
  });

  it("should NOT transfer owner", async () => {
    // NOT owner
    await expectRevert(
      token.changeIssuer(accounts[1], { from: accounts[1] }),
      "Caller is not the issuer"
    );
  
    // Empty Address
    await expectRevert(
      token.changeIssuer(ZERO_ADDRESS, { from: accounts[0] }),
      "Empty address is not allowed"
    );
  });

  it("should issue token", async () => {
    const receipt = await token.issueToken(accounts[1], 'Token0URI', { from: accounts[0] });
    const balance_1 = await token.balanceOf(accounts[1]);
    const token0Owner = await token.ownerOf(0);
    const token0URI = await token.tokenURI(0);

    expectEvent(receipt, "Transfer", { 
      from: ZERO_ADDRESS, 
      to: accounts[1],
      tokenId: "0"
    });
    assert.equal(balance_1.toNumber(), 1);
    assert.equal(token0Owner, accounts[1]);
    assert.equal(token0URI, 'Token0URI');

    // console.log(receipt);
  });

  it("should NOT issue token", async () => {
    // NOT issuer
    await expectRevert(
      token.issueToken(accounts[1], 'Token0URI', { from: accounts[1] }),
      "Caller is not the issuer"
    );

    // to empty address
    await expectRevert(
      token.issueToken(ZERO_ADDRESS, 'Token0URI', { from: accounts[0] }),
      "ERC721: mint to the zero address"
    );
  });

  it("should issue multiple tokens", async () => {
    // about 100 Tokens (0 ~ 99)
    const owners = new Array(100);
    const tokenURIs = new Array(100);
    for(let i = 0; i < 100; i++) {
      owners[i] = accounts[i % 10];
      tokenURIs[i] = `Token${i}URI`;
    }
    
    // console.log("OWNERS::");
    // owners.forEach((el, idx) => console.log(idx, ": ", el));
    // console.log("URIS::");
    // tokenURIs.forEach((el, idx) => console.log(idx, ": ", el));

    const receipt = await token.issueMultipleTokens(owners, tokenURIs);
    for(let i = 0; i < 100; i++) {
      expectEvent(receipt, "Transfer", { 
        from: ZERO_ADDRESS, 
        to: owners[i], 
        tokenId: String(i)
      });
    }

    console.log("Gas Used: ", receipt.receipt.gasUsed);

    const tokenIds = new Array(100).fill().map((el, idx) => el = idx);

    const getOwners = await Promise.all(tokenIds.map((el, idx) => token.ownerOf(idx)));
    const getURIs = await Promise.all(tokenIds.map((el, idx) => token.tokenURI(idx)));
    const ownerBalance = await Promise.all(accounts.map(_account => token.balanceOf(_account)));

    for(let i = 0; i < 100; i++) {
      assert.equal(getOwners[i], owners[i]);
      assert.equal(getURIs[i], tokenURIs[i]);
    }
    for(let i = 0; i < 10; i++) {
      assert.equal(ownerBalance[i].toNumber(), 10);
    }
  });

  // it("transfer test", async () => {
  //   const owners = new Array(100);
  //   const ownersTo = new Array(100);
  //   const tokenURIs = new Array(100);
  //   for(let i = 0; i < 100; i++) {
  //     owners[i] = accounts[0];
  //     ownersTo[i] = accounts[1];
  //     tokenURIs[i] = `Token${i}URI`;
  //   }

  //   const receipt = await token.issueMultipleTokens(owners, tokenURIs);

  //   console.log("ISSUEING:: ", receipt.receipt.gasUsed);

  //   const ids = new Array(100).fill().map((el, idx) => el = idx);

  //   const receipt2 = await token.transferMultipleTokens(ownersTo, ids);

  //   console.log("TRANSFERERIGN :: " , receipt2.receipt.gasUsed);

  //   console.log(owners)

  //   console.log(tokenURIs);
  // });

  it("should NOT issue multiple tokens", async () => {
    const recipients = new Array(10).fill().map((el, idx) => el = accounts[idx]);
    
    // different input length
    const invalidLengthUris = new Array(9).fill().map((el, idx) => el = `URI${idx}`);
    
    await expectRevert(
      token.issueMultipleTokens(recipients, invalidLengthUris),
      "Input length not same"
    );

    // not owner
    const validUris = new Array(10).fill().map((el, idx) => el = `URI${idx}`);

    await expectRevert(
      token.issueMultipleTokens(recipients, validUris, { from: accounts[1] }),
      "Caller is not the issuer"
    );

    // one of addresses is empty
    const invalidAddresses = new Array(10).fill().map((el, idx) => el = accounts[idx]);
    invalidAddresses[4] = ZERO_ADDRESS;

    console.log(invalidAddresses);
    
    await expectRevert(
      token.issueMultipleTokens(invalidAddresses, validUris, { from: accounts[0] }),
      "ERC721: mint to the zero address"
    );
  });

  it("transfer multiple tokens", async () => {
    const owners = new Array(100);
    const tokenURIs = new Array(100);
    for(let i = 0; i < 100; i++) {
      owners[i] = accounts[0];
      tokenURIs[i] = `Token${i}URI`;
    }

    await token.issueMultipleTokens(owners, tokenURIs, { from: accounts[0] });
    const beforeBalance_0 = await token.balanceOf(accounts[0]);
    const beforeBalance_1 = await token.balanceOf(accounts[1]);

    const toOwners = new Array(100).fill().map(el => el = accounts[1]);
    const tokenIds = new Array(100).fill().map((el, idx) => el = idx);

    const receipt = await token.transferMultipleTokens(toOwners, tokenIds, { from: accounts[0] });
    const tokenOwners = await Promise.all(tokenIds.map((el, idx) => token.ownerOf(idx)));
    const afterBalance_0 = await token.balanceOf(accounts[0]);
    const afterBalance_1 = await token.balanceOf(accounts[1]);

    for(let i = 0; i < 100; i++) {
      expectEvent(receipt, "Transfer", { 
        from: owners[i], 
        to: toOwners[i], 
        tokenId: toBN(String(i)) 
      });
    }
    assert.equal(beforeBalance_0.toNumber(), 100);
    assert.equal(beforeBalance_1.toNumber(), 0);
    tokenOwners.map(el => assert.equal(el, accounts[1]));
    assert.equal(afterBalance_0.toNumber(), 0);
    assert.equal(afterBalance_1.toNumber(), 100);
  });

  it("should NOT transfer multiple tokens", async () => {
    // token does not exist
    await expectRevert(
      token.transferMultipleTokens([accounts[1], accounts[2]], [0 ,1], { from: accounts[0] }),
      "ERC721: owner query for nonexistent token" // transfer checks owner first
    );

    const owners = new Array(100);
    const tokenURIs = new Array(100);
    for(let i = 0; i < 100; i++) {
      owners[i] = accounts[0];
      tokenURIs[i] = `Token${i}URI`;
    }
    await token.issueMultipleTokens(owners, tokenURIs, { from: accounts[0] });

    // not token owner
    const tokenIds = new Array(100).fill().map((el, idx) => el = idx);
    const toOwners = new Array(100).fill(accounts[1]);

    await expectRevert(
      token.transferMultipleTokens(toOwners, tokenIds, { from: accounts[1] }),
      "ERC721: transfer of token that is not own"
    );

    // different input length
    const invalidLengthIds = new Array(99).fill().map((el, idx) => el = idx);
    
    await expectRevert(
      token.transferMultipleTokens(owners, invalidLengthIds, { from: accounts[0] }),
      "Input length not same"
    );

    // one of address is empty
    const invalidOwners = new Array(100).fill(accounts[1]);
    invalidOwners[23] = ZERO_ADDRESS;

    await expectRevert(
      token.transferMultipleTokens(invalidOwners, tokenIds, { from: accounts[0] }),
      "ERC721: transfer to the zero address"
    );
  });

  // ERC721 Part
  describe("functions ERC721 standard", () => {
    beforeEach(async () => {
      await token.issueToken(accounts[1], "Token0", { from: accounts[0] });
    });

    it("should issue token successfully", async () => {
      const balance_1 = await token.balanceOf(accounts[1]);
      const owner_token0 = await token.ownerOf(0);
      const tokenURI_token0 = await token.tokenURI(0);

      assert.equal(balance_1.toNumber(), 1);
      assert.equal(owner_token0, accounts[1]);
      assert.equal(tokenURI_token0, "Token0");
    });

    it("should transfer token", async () => {
      const receipt = await token.transferFrom(accounts[1], accounts[2], 0, { from: accounts[1] });
      
      const afterBalance_1 = await token.balanceOf(accounts[1]);
      const afterBalance_2 = await token.balanceOf(accounts[2]);
      const afterOwner_token0 = await token.ownerOf(0);

      expectEvent(receipt, "Transfer", {
        from: accounts[1],
        to: accounts[2],
        tokenId: "0"
      });
      assert.equal(afterBalance_1.toNumber(), 0);
      assert.equal(afterBalance_2.toNumber(), 1);
      assert.equal(afterOwner_token0, accounts[2]);
    });

    it("should transferFrom token", async () => {
      const receipt = await token.approve(accounts[2], 0, { from: accounts[1] });

      await token.transferFrom(accounts[1], accounts[2], 0, { from: accounts[2] });
      const afterBalance_1 = await token.balanceOf(accounts[1]);
      const afterBalance_2 = await token.balanceOf(accounts[2]);
      const afterOwner_token0 = await token.ownerOf(0);

      expectEvent(receipt, "Approval", { 
        owner: accounts[1], 
        approved: accounts[2],
        tokenId: "0"
      });
      assert.equal(afterBalance_1.toNumber(), 0);
      assert.equal(afterBalance_2.toNumber(), 1);
      assert.equal(afterOwner_token0, accounts[2]);
    });

    it("should transferFrom token with approveAll", async () => {
      await token.issueToken(accounts[1], "Token1", { from: accounts[0] });
      
      const beforeBalance_1 = await token.balanceOf(accounts[1]);
      
      const receipt = await token.setApprovalForAll(accounts[2], true, { from: accounts[1] });

      await token.transferFrom(accounts[1], accounts[2], 0, { from: accounts[2] });
      await token.transferFrom(accounts[1], accounts[2], 1, { from: accounts[2] });
      
      const afterBalance_1 = await token.balanceOf(accounts[1]);
      const afterBalance_2 = await token.balanceOf(accounts[2]);
      const afterOwner_token0 = await token.ownerOf(0);
      const afterOwner_token1 = await token.ownerOf(1);

      expectEvent(receipt, "ApprovalForAll", { 
        owner: accounts[1], 
        operator: accounts[2],
        approved: true
      });
      assert.equal(afterBalance_1.toNumber(), 0);
      assert.equal(afterBalance_2.toNumber(), 2);
      assert.equal(afterOwner_token0, accounts[2]);
      assert.equal(afterOwner_token1, accounts[2]);
    });
    
    it("should NOT transfer", async () => {
      // caller is not owner
      await expectRevert(
        token.transferFrom(accounts[1], accounts[2], 0, { from: accounts[2] }),
        "ERC721: transfer caller is not owner nor approved"
      );

      // not exist token
      await expectRevert(
        token.transferFrom(accounts[1], accounts[2], 1, { from: accounts[1] }),
        "ERC721: operator query for nonexistent token"
      );
    });

    it("should NOT approve", async () => {
      // not owner
      await expectRevert(
        token.approve(accounts[1], 0, { from: accounts[1] }),
        "ERC721: approval to current owner"
      );

      // owner approves owner
      await expectRevert(
        token.approve(accounts[0], 0, { from: accounts[0]} ),
        "ERC721: approve caller is not owner nor approved for all"
      );

      // not exist
      await expectRevert(
        token.approve(accounts[1], 1, { from: accounts[0] }),
        "ERC721: owner query for nonexistent token"
      );
    });

    it.only("should NOT approvalForAll", async () => {
      // caller approves caller
      await expectRevert(
        token.setApprovalForAll(accounts[0], true, { from: accounts[0] }),
        "ERC721: approve to caller"
      );
    });
  });
});