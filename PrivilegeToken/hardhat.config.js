require("@nomiclabs/hardhat-ethers");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const url = "https://kovan.infura.io/v3/8a313bcfaadd4f6384598e705151f606";
const prv = "f7c9a8bd547b9255eca7648e81ba3b2046ab40550f74de95344a1133fc1a4684";

module.exports = {
  solidity: "0.8.0",

  networks: {
    kovan: {
      url,
      accounts: [prv]
    },

    development: {
      url: "http://127.0.0.1:8545",
      accounts: [prv]
    }
  }
};
