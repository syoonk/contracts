const { merge } = require('sol-merger');

const merging = async () => {
// Get the merged code as a string
  const code = await merge("./contracts/PrivilegeToken.sol");
// // Print it out or write it to a file etc.

  console.log(code);

}

merging();