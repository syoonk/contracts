pragma solidity ^0.8.0;

contract IssuerManager {
  
  address private _issuer;

  event IssuerTransferred(address indexed previousIssuer, address indexed newIssuer);

  constructor() {
    _issuer = msg.sender;

    emit IssuerTransferred(address(0), msg.sender);
  }

  modifier onlyIssuer() {
    require(msg.sender == _issuer,
      "Caller is not the issuer");
    _;
  }

  function changeIssuer(address _newIssuer)
  external
  onlyIssuer() {
    require(_newIssuer != address(0),
      "Empty address is not allowed");

    address previousIssuer = _issuer;
    _issuer = _newIssuer;

    emit IssuerTransferred(previousIssuer, _newIssuer);
  }

  function issuer()
  external view
  returns(address) {
    return _issuer;
  }

}