pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "./IssuerManager.sol";

contract PrivilegeToken is ERC721URIStorage, IssuerManager {
  
  using SafeMath for uint;
  
  uint private _nextTokenId = 0;

  string private _uri;

  constructor()
  ERC721("Privilege Token", "PNFT")
  IssuerManager() {}

  function issueToken(address _recipient, string memory _tokenUri)
  public 
  onlyIssuer() {
    _mint(_recipient, _nextTokenId);

    _setTokenURI(_nextTokenId, _tokenUri);

    _nextTokenId = _nextTokenId.add(1);
  }

  function issueMultipleTokens(address[] calldata _recipients, string[] calldata _tokenUris)
  public
  onlyIssuer() {
    require(_recipients.length == _tokenUris.length,
      "Input length not same");
    
    for(uint i = 0; i < _recipients.length; i++) {
      issueToken(_recipients[i], _tokenUris[i]);
    }
  }

  function transferMultipleTokens(address[] calldata _to, uint[] calldata _tokenIds)
  public {
    require(_to.length == _tokenIds.length,
      "Input length not same");

    for(uint i = 0; i < _to.length; i++) {
      _transfer(msg.sender, _to[i], _tokenIds[i]);
    }
  }

  function setTokenURI(uint _tokenId, string memory _tokenUri)
  public
  onlyIssuer() {
    _setTokenURI(_tokenId, _tokenUri);
  }

  function setBaseURI(string calldata _newURI)
  external
  onlyIssuer() {
    _uri = _newURI;
  }

  function nextTokenId()
  external view
  returns(uint) {
    return _nextTokenId;
  }

  function baseURI()
  external view
  returns(string memory) {
    return _baseURI();
  }

  function _baseURI()
  internal view override
  returns(string memory) {
    return _uri;
  }

}