const main = async () => {
  // We get the contract to deploy
  const Token = await ethers.getContractFactory("PrivilegeToken");
  const token = await Token.deploy();

  console.log("Greeter deployed to:", token.address);
}

main()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });